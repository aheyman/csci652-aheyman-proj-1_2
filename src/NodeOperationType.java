public enum NodeOperationType{

    NODE_JOIN,
    NODE_DEPARTURE,
    PUSH_FILE_TO_REMOTE,
    KEY_TRANSFER,
    FILE_SEARCH,
    RECEIVE_FILE

}