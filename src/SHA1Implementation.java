import java.math.BigInteger;
import java.security.MessageDigest;

public class SHA1Implementation {

    final static public int NUMBER_OF_BITS_IN_ID = (int)Math.pow(2,7);

    public static long hashFunction(String hashString){

        try{
            MessageDigest digest = MessageDigest.getInstance("SHA-1");

            digest.update(hashString.getBytes());
            BigInteger bi = new BigInteger(digest.digest());
            long val = bi.mod(BigInteger.valueOf(NUMBER_OF_BITS_IN_ID)).longValue();
            return val;

        } catch (Exception e){
            e.printStackTrace();
        }

        return -1L;
    }

    public static void main(String[] args){
        System.out.println(SHA1Implementation.hashFunction("127.0.0.1:9000"));
    }
}
