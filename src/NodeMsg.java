import javafx.util.Pair;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.List;

class NodeMsg {

    private NodeOperationType op;
    private Pair<String, String>[] files;
    private String requestIPAddress;
    private int requestListenPort;
    private long fileHash;

    NodeMsg(NodeOperationType op, String requestIPAddress, int requestListenPort){
        this.op = op;
        this.requestIPAddress = requestIPAddress;
        this.requestListenPort = requestListenPort;
    }

    NodeOperationType getOp() {
        return op;
    }

    void setFileBase64(List<File> files, long fileHash){

        this.fileHash = fileHash;
        this.files = new Pair[files.size()];

        for(int i = 0; i < files.size(); i++){
            try{
                this.files[i] = new Pair<>(files.get(i).getName(), Base64.getEncoder().encodeToString(Files.readAllBytes(Paths.get(files.get(i).getPath()))));
            } catch (Exception e) {
            }
        }
    }

    void setFileBase64(Pair<String, String>[] files, long fileHash){

        this.fileHash = fileHash;
        this.files = files;

    }

    Pair<String, String>[] getFilesBase64(){
        return this.files;
    }

    String getRequestIPAddress() {
        return requestIPAddress;
    }

    int getRequestListenPort() {
        return requestListenPort;
    }

    void setFileHash(long fileHash){
        this.fileHash = fileHash;
    }

    long getFileHash() {
        return fileHash;
    }
}
