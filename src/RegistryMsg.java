import java.io.Serializable;

public class RegistryMsg implements Serializable {

    private RegisterOperation op;
    private int commPort;

    public RegistryMsg(final RegisterOperation op, int commPort){
        this.op = op;
        this.commPort = commPort;
    }

    public RegisterOperation getOperation(){
        return op;
    }

    public int getPort(){return this.commPort;}



}
