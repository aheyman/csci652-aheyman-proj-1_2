import com.google.gson.Gson;
import javafx.util.Pair;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Node {

    // Per node thread pool
    private final ExecutorService threadPool = Executors.newFixedThreadPool(5); // Listening and Sending

    // local file directories
    private final String remoteFilePath = "/root/ChordFiles";
    private final String localFilePath = "/root/ChordFiles";

    // Registry
    private String nameService;
    private int namePort;

    // Node attributes
    private String nodeIPAddress;
    private int nodeListenPort;
    private long nodeGUID;
    private FingerTableEntry predecessor;

    // Finger Tables
    private FingerTableEntry[] fingerTable;
    private final static Object fingerLock = new Object();
    private int logNodes;

    // Listening Thread for safe shutdown
    private Thread serverThread;


    public Node(String nameService, int namePort, int nodeListenPort){

        this.nameService = nameService;
        this.namePort = namePort;
        this.nodeListenPort = nodeListenPort;
        setIpAddress();
        this.nodeGUID = SHA1Implementation.hashFunction(this.nodeIPAddress + ":"+ this.nodeListenPort);
        System.out.println("This node hash is: " + nodeGUID);

        // Create the file directories
        createDirectories();

        // Begin listening
        startListener(nodeListenPort);

        // Register with the service
        threadPool.submit(registerCall(RegisterOperation.REGISTER_ONLINE));

    }

    private void setIpAddress(){
        String interfaceName = "eth0";
        NetworkInterface networkInterface = null ;
        try {
            networkInterface = NetworkInterface.getByName(interfaceName);
        } catch (Exception e){}
        Enumeration<InetAddress> inetAddress = networkInterface.getInetAddresses();
        InetAddress currentAddress;
        while(inetAddress.hasMoreElements())
        {
            currentAddress = inetAddress.nextElement();
            if(currentAddress instanceof Inet4Address && !currentAddress.isLoopbackAddress())
            {
                this.nodeIPAddress = currentAddress.toString().substring(1, currentAddress.toString().length());
                System.out.println("IP FINDER " + this.nodeIPAddress);
                break;
            }
        }
    }

    /**
     * Sets up the directories on the current node
     */
    private void createDirectories(){
        File dir = new File(remoteFilePath);
        File dir2 = new File (localFilePath);

        if (!dir.exists()){
            dir.mkdir();
        }

        if (!dir2.exists()){
            dir2.mkdir();
        }
    }


    /**
     * Thread listening for inbound connections
     * @param listenPort
     */
    private void startListener(int listenPort){

        Runnable serverTask = () -> {
            try {
                ServerSocket serverSocket = new ServerSocket(listenPort);

                while (!Thread.currentThread().isInterrupted()) {
                    Socket clientSocket = serverSocket.accept();
                    threadPool.submit(new InboundConnection(clientSocket));
                }
            } catch (IOException e) {
                System.out.println("Issue with the request");
                e.printStackTrace();
            }
        };
        serverThread = new Thread(serverTask);
        serverThread.start();
    }

    /**
     * Creates finger table
     * @param onlineNodes list of online nodes
     */
    private void generateFingerTable(List onlineNodes){

        int numberOfNodes = Integer.parseInt((String)onlineNodes.get(0));
        logNodes = (int)Math.ceil(Math.log(numberOfNodes)/Math.log(2));

        if (onlineNodes.size() == 2){ // Nodes and itself
            predecessor = new FingerTableEntry(this.nodeIPAddress, this.namePort,
                    SHA1Implementation.hashFunction(this.nodeIPAddress + ":"+this.namePort));
            initializeFingerTable(logNodes);
        } else {
            updateFingerTables(onlineNodes);
        }
    }

    /**
     * Helper method for finger table creation
     * @param logNodes
     */
    private void initializeFingerTable(int logNodes){
        synchronized (fingerLock){
            fingerTable = new FingerTableEntry[logNodes];
            FingerTableEntry et = new FingerTableEntry(this.nodeIPAddress, this.nodeListenPort, this.nodeGUID);
            for (int i = 0; i < logNodes; i++){
                fingerTable[i] = et;
            }
        }
    }

    /**
     * Update finger table
     * @param onlineNodes
     */
    private void updateFingerTables(List onlineNodes){

        List<Triplet<String, Integer, Long>> shaValues = new ArrayList<>();

        for (int i = 1; i < onlineNodes.size(); i++) {
            String val = ((String) onlineNodes.get(i)).substring(1, ((String) onlineNodes.get(i)).length()); // parse out the '/'
            String[] values = val.split(":");
            shaValues.add(new Triplet<>(values[0], Integer.parseInt(values[1]),
                    SHA1Implementation.hashFunction(val)));
        }

        shaValues.sort(Comparator.comparing(Triplet::getThird));

        Optional<Triplet<String, Integer, Long>> pred =  shaValues.stream().filter(x -> x.third < this.nodeGUID).max(Comparator.comparing(Triplet::getThird));
        if (!pred.isPresent()){
            pred = shaValues.stream().filter(x -> x.third >= this.nodeGUID).max(Comparator.comparing(Triplet::getThird));
        }
        predecessor = new FingerTableEntry(pred.get().first, pred.get().second, pred.get().third);

        synchronized (fingerLock) {
           initializeFingerTable(logNodes);

            // For the table entries, update the values to the online nodes
            for (int i = 0; i < logNodes; i++) {
                Long in = (this.nodeGUID + (long) (Math.pow(4, i ))) % (long) SHA1Implementation.NUMBER_OF_BITS_IN_ID;
                Optional<Triplet<String, Integer, Long>> result =  shaValues.stream().filter(x -> x.third >= in).min(Comparator.comparing(Triplet::getThird));
                if (result.isPresent()){
                    fingerTable[i] = new FingerTableEntry(result.get().first, result.get().second, result.get().third);
                } else{
                    result =  shaValues.stream().filter(x -> x.third <= in).min(Comparator.comparing(Triplet::getThird));
                    fingerTable[i] = new FingerTableEntry(result.get().first, result.get().second, result.get().third);
                }
            }
        }
    }


    /**
     * Send JSON message to peers
     * @param peers
     * @param op
     */
    private void informPeers(List peers, NodeOperationType op){

        Gson g = new Gson();
        for (int i = 1; i < peers.size(); i++){
            if (((String) peers.get(i)).contains(nodeIPAddress + ":" + nodeListenPort)) {
                continue;
            }
            String val = ((String) peers.get(i)).substring(1, ((String) peers.get(i)).length()); // parse out the '/'
            String[] values = val.split(":");

            NodeMsg msg = new NodeMsg(op, this.nodeIPAddress, this.nodeListenPort);
            sendJson(g.toJson(msg), values[0], Integer.parseInt(values[1]));
        }
    }

    /**
     * Adds the file to the system
     * @param file
     */
    private void putFile(File file){

        Gson g = new Gson();
        Long hashVal = SHA1Implementation.hashFunction(file.getName());
        System.out.println("Hash for the file: " + hashVal);
        NodeMsg msg = new NodeMsg(NodeOperationType.PUSH_FILE_TO_REMOTE, this.nodeIPAddress, this.nodeListenPort);
        msg.setFileBase64(Arrays.asList(file), hashVal );
        sendJson(g.toJson(msg), getNode(hashVal));

    }

    /**
     * Helper method for sending JSON strings
     * @param json
     * @param ipaddress
     * @param port
     */
    private void sendJson(String json, String ipaddress, int port){
        sendJson(json, new FingerTableEntry(ipaddress, port,-1));
    }

    /**
     * Send JSON to FingerTable
     * @param json
     * @param entry
     */
    private void sendJson(String json, FingerTableEntry entry){
        Runnable sendJson = () -> {
            try {
                Socket socket = new Socket(entry.getIpAddress(), entry.getPort());
                ObjectOutputStream stream = new ObjectOutputStream(socket.getOutputStream());
                stream.writeObject(json);
                stream.close();
                socket.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        };
        threadPool.submit(sendJson);
    }

    /**
     * Find the finger table entry for a specific hash value
     * @param hashVal
     * @return
     */
    private FingerTableEntry getNode(long hashVal) {

        Optional<FingerTableEntry> rightSide = Arrays.stream(fingerTable)
                .filter(x -> x.hashCode() > hashVal)
                .min(Comparator.comparing(z -> z.hashCode()));

        return rightSide.orElseGet(() -> Arrays.stream(fingerTable)
                .filter(x -> x.hashCode() <= hashVal)
                .max(Comparator.comparing(FingerTableEntry::hashCode)).get());
    }

    /**
     * Find the correct node that contains a filename
     * @param filename
     * @return
     */
    private void locateFile(String filename){

        long hashVal = SHA1Implementation.hashFunction(filename);

        // Is it on supposed to be on this node?
        if (hashVal > predecessor.id && hashVal <= this.nodeGUID){
            File[] files = new File(remoteFilePath).listFiles();
            for (File file : files){
               if (file.getName().toLowerCase().equals(filename.toLowerCase())){
                    System.out.println("File is located in " + remoteFilePath);
                } else{
                    System.out.println("File does not exist on the network");
                }
            }
        } else{
            NodeMsg msg = new NodeMsg(NodeOperationType.FILE_SEARCH, this.nodeIPAddress, this.nodeListenPort);
            msg.setFileHash(hashVal);
            Gson g = new Gson();
            sendJson(g.toJson(msg), getNode(hashVal));
        }
    }

    /**
     * Moves keys to the successor node
     */
    private void transferKeys() {
        NodeMsg msg = new NodeMsg(NodeOperationType.KEY_TRANSFER, this.nodeIPAddress, this.nodeListenPort);
        File[] files = new File(remoteFilePath).listFiles();
        if (files != null && files.length > 0)  {
            long hashVal = SHA1Implementation.hashFunction(files[0].getName());
            msg.setFileBase64(Arrays.asList(files),hashVal);
            Runnable keyTransfer = () -> {
                try {
                    Gson g = new Gson();
                    FingerTableEntry entry = getNode(hashVal);
                    if (entry.id == this.nodeGUID) {
                        System.out.println("Cannot transfer keys without more nodes on the network");
                    } else {
                        Socket socket = new Socket(entry.getIpAddress(), entry.port);
                        ObjectOutputStream stream = new ObjectOutputStream(socket.getOutputStream());
                        stream.writeObject(g.toJson(msg));
                        stream.close();
                        socket.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            };

            Thread tran = new Thread(keyTransfer);
            tran.start();
        }
    }

    /**
     * Perform a registry operation (i.e. online, offline, query)
     * @param op
     * @return
     */
    private Runnable registerCall(RegisterOperation op){

        Runnable register = () -> {
            try {
                Socket socket = new Socket(nameService, namePort);
                ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
                out.writeObject(new RegistryMsg(op, this.nodeListenPort));
                Object obj = new ObjectInputStream(socket.getInputStream()).readObject();
                out.close();
                socket.close();

                switch (op){
                    case REGISTER_ONLINE:
                        generateFingerTable((List)obj);
                        informPeers((List)obj, NodeOperationType.NODE_JOIN);
                        break;
                    case REGISTER_OFFLINE:
                        informPeers((List)obj, NodeOperationType.NODE_DEPARTURE);
                    case QUERY:
                        updateFingerTables((List)obj);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        };

        return register;
    }

    /**
     * Shutdown hook
     */
    private void shutdown(){
        transferKeys();
        Thread lastThread = new Thread(registerCall(RegisterOperation.REGISTER_OFFLINE));
        lastThread.run();
        serverThread.interrupt();
        //threadPool.shutdown();
        try{
            threadPool.awaitTermination(5000,TimeUnit.MILLISECONDS);
            while(lastThread.isAlive()){
                Thread.currentThread().sleep(1000);
            }
        } catch (Exception e){}

    }

    /**
     * Class to handle inbound messages
     */
    private class InboundConnection implements Runnable {

        ObjectInputStream inputStream;

        private InboundConnection(Socket clientSocket) throws IOException {
            inputStream = new ObjectInputStream(clientSocket.getInputStream());
        }
        @Override
        public void run() {

            // A connection has been made, perform the operation
            try {
                Object o = inputStream.readObject();
                Gson g = new Gson();

                if (o instanceof String){
                    NodeMsg msg = g.fromJson((String)o, NodeMsg.class);
                    switch(msg.getOp()){
                        case NODE_JOIN:
                        case NODE_DEPARTURE:
                            threadPool.submit(registerCall(RegisterOperation.QUERY));
                            break;

                        case FILE_SEARCH:
                            long fileHash = msg.getFileHash();
                            fileSearch(fileHash, msg, g);
                            break;
                        case PUSH_FILE_TO_REMOTE:
                            fileHash = msg.getFileHash();
                            submitFileToRemote(fileHash, msg, g);
                            break;
                        case KEY_TRANSFER:
                            writeFile(remoteFilePath, msg);
                            break;
                        case RECEIVE_FILE:
                            if (msg.getFilesBase64() != null){
                                writeFile(localFilePath, msg);
                            } else{
                                System.out.println("File not on network");
                            }
                            break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /**
         * Search for a file
         * @param fileHash
         * @param msg
         * @param g
         */
        private void fileSearch(long fileHash, NodeMsg msg, Gson g){

            // If it's on this node
            if (fileHash <= nodeGUID && fileHash < predecessor.id){
                File f = new File(remoteFilePath + msg.getFilesBase64()[0].getKey());
                // Send receive file with file
                if (f.exists()){
                    NodeMsg newMsg = new NodeMsg(NodeOperationType.RECEIVE_FILE,nodeIPAddress, nodeListenPort);
                    newMsg.setFileBase64(Arrays.asList(f), fileHash);
                    sendJson(g.toJson(newMsg),msg.getRequestIPAddress(), msg.getRequestListenPort());

                } else{ // Send null receive file
                    NodeMsg newMsg = new NodeMsg(NodeOperationType.RECEIVE_FILE,nodeIPAddress, nodeListenPort);
                    sendJson(g.toJson(newMsg),msg.getRequestIPAddress(), msg.getRequestListenPort());
                }
            } else{ // Propagate
                    FingerTableEntry entry = getNode(fileHash);
                    NodeMsg newMsg = new NodeMsg(NodeOperationType.RECEIVE_FILE,msg.getRequestIPAddress(), msg.getRequestListenPort());
                    msg.setFileHash(fileHash);
                    sendJson(g.toJson(newMsg),entry);
                }
        }


        /**
         * Commit file to network
         * @param fileHash
         * @param msg
         * @param g
         */
        private void submitFileToRemote(long fileHash, NodeMsg msg, Gson g){
            // If it's on this node
            if (fileHash  <= nodeGUID && fileHash > predecessor.id){
                writeFile(remoteFilePath, msg);
            }else {
                NodeMsg newMsg = new NodeMsg(NodeOperationType.PUSH_FILE_TO_REMOTE,msg.getRequestIPAddress(), msg.getRequestListenPort());
                newMsg.setFileBase64(msg.getFilesBase64(), msg.getFileHash());
                sendJson(g.toJson(newMsg), getNode(fileHash));
            }
        }

        private void writeFile(String fileLocation, NodeMsg msg){
            Pair<String, String>[] files = msg.getFilesBase64();
            for (Pair<String, String> file : files){
                byte[] decoded = Base64.getDecoder().decode(file.getValue());
                try {
                    System.out.println("Saving file: " + fileLocation +"/" + file.getKey());
                    FileOutputStream stream = new FileOutputStream(fileLocation +"/" + file.getKey());
                    stream.write(decoded);
                    stream.close();
                } catch (Exception e){ }
            }
        }
    }

    public static void main(String[] args){

        int port = 9000;
        boolean shutdown = false;
        String registryAddress = "localhost";

        if (args.length > 0){
            registryAddress = "172.17.0.2";
            port = Integer.parseInt(args[1]);
        }

        Node node = new Node(registryAddress, 8000, port );
        Scanner sc = new Scanner(System.in);

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.out.println("Shutting Down");
            node.shutdown();
        }));

        System.out.println("Node running, what would you like to do?");

        while (!shutdown){

            System.out.println("======================");
            System.out.println("1. Add a file");
            System.out.println("2. Retrieve a file");
            System.out.println("3. View Finger Tables");
            System.out.println("4. Shutdown");

            String line = sc.nextLine();
            Integer input;
            try {
                input = Integer.parseInt(line);
            } catch (NumberFormatException e){
                input = -1;
            }


            switch(input){

                case 1:
                    System.out.println("File path for file?");
                    String path = sc.nextLine();
                    node.putFile(new File(path));
                    break;
                case 2:
                    System.out.println("File name?");
                    String name = sc.nextLine();
                    node.locateFile(name);
                    break;
                case 3:
                    for (FingerTableEntry entry : node.fingerTable){
                        System.out.println(entry);
                    }
                    break;
                case 4:
                    System.out.println("Goodbye!");
                    sc.close();
                    shutdown = true;
                    break;
                default:
                    break;

            } // Switch
        }// While

        System.exit(0);
    }// Main
}// Class
