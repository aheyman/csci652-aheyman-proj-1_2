
public class FingerTableEntry{

    String ipAddress;
    int port;
    long id;

    public  FingerTableEntry(final String address, final int port, long GUID){

        this.ipAddress = address;
        this.port = port;
        this.id = GUID;
    }

    public String getIpAddress() {
        return ipAddress;
    }
    public int getPort() {
        return port;
    }
    public long getId(){
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof FingerTableEntry){
            FingerTableEntry that = (FingerTableEntry)obj;
            return that.ipAddress.equals(this.ipAddress) && that.port == this.port;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Long.hashCode(id);
    }

    public String toString(){
        return ipAddress + ":" + port + ", " + id;

    }

}