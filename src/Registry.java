import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Registry {

    final private ExecutorService threadPool = Executors.newFixedThreadPool(10);
    final private static int NUMBER_OF_NODES = 16;
    private static Registry instance = null;
    private static List<String> onlineNodes = new ArrayList<>(Arrays.asList("" + NUMBER_OF_NODES));

    private Registry() {
    }

    private static Registry getInstance() {

        if (instance == null) {
            instance = new Registry();
        }
        return instance;
    }

    private void start(int port) {

        System.out.println("Starting Registry on port:" + port);

        Runnable serverTask = () -> {
            try {
                ServerSocket serverSocket = new ServerSocket(port);
                System.out.println(serverSocket.getInetAddress());

                while (true) {
                    Socket clientSocket = serverSocket.accept();
                    threadPool.submit(new ConnectionTask(clientSocket));
                }
            } catch (IOException e) {
                System.out.println("Issue with the request");
                e.printStackTrace();
            }
        };
        Thread serverThread = new Thread(serverTask);
        serverThread.start();
    }

    private class ConnectionTask implements Runnable {
        private final Socket clientSocket;
        ObjectInputStream inputStream;


        private ConnectionTask(Socket clientSocket) throws IOException {
            this.clientSocket = clientSocket;
            inputStream = new ObjectInputStream(clientSocket.getInputStream());
        }

        @Override
        public void run() {
            // A connection has been made, perform the operation
            try {
                Object o = inputStream.readObject();

                if (o instanceof RegistryMsg) {
                    String nodeInfo = clientSocket.getInetAddress().toString()+":"+((RegistryMsg) o).getPort();
                    ObjectOutputStream out;

                    switch (((RegistryMsg)o).getOperation()){
                        case REGISTER_ONLINE:
                            System.out.println("Node registering: " + nodeInfo);
                            onlineNodes.add(nodeInfo);
                            break;

                        case REGISTER_OFFLINE:
                            System.out.println("Node Leaving: " + nodeInfo);
                            onlineNodes.remove(nodeInfo);
                            break;
                    }

                    out = new ObjectOutputStream(clientSocket.getOutputStream());
                    out.writeObject(onlineNodes);
                    clientSocket.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {

        Registry reg = Registry.getInstance();

        if (args.length == 0) {
            reg.start(8000);
        } else {
            reg.start(Integer.parseInt(args[0]));
        }
    }
}
