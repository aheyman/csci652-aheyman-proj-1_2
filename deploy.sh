#!/usr/bin/env bash
# This should be performed initially
# git clone https://gitlab.com/aheyman/csci652-aheyman-proj-1_2.git
cd ..

for count in $(seq 16)
do
    if [ $count -eq 1 ]
    then
        label="aheymanRegistry"
    else
        label="aheyman$count"
    fi
    echo "LABEL = $label"
	# Start the container
	docker run -d --name=$label -it peiworld/csci652
	
	# Copy the source code
	docker cp csci652-aheyman-proj-1_2 $label:/

	# Find the source files
	docker exec -d $label  sh -c "find / -type f -wholename  '/csci652-aheyman-proj-1_2/src/*.java' > /csci652-aheyman-proj-1_2/source.txt"

	# Make a build directory 
	docker exec -d $label  sh -c "mkdir /csci652-aheyman-proj-1_2/build"
		
	# Build
	docker exec -d $label sh -c "cd /csci652-aheyman-proj-1_2/ && javac -cp .:lib/gson-2.8.2.jar -d /csci652-aheyman-proj-1_2/build @source.txt"

	if [ $count -eq 1 ]
    then
        docker exec -d $label sh -c "cd /csci652-aheyman-proj-1_2/build/ && java -cp .:../lib/gson-2.8.2.jar Registry"
    else
        docker exec -d $label sh -c "cd /csci652-aheyman-proj-1_2/build/ && java -cp .:../lib/gson-2.8.2.jar Node 172.17.0.2 9000"
    fi

done

## if the exec fails, use
# docker attach aheymanRegistry
# cd /csci652-aheyman-proj-1_2/build/ && java -cp .:../lib/gson-2.8.2.jar Registry


# docker attach aheyman2
# cd /csci652-aheyman-proj-1_2/build/ && java -cp .:../lib/gson-2.8.2.jar Node 172.17.0.2 9000